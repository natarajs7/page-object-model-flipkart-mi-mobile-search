package pagesFlipkart;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class PopUpPage extends ProjectMethods
{


	public PopUpPage() 
	{
		PageFactory.initElements(driver, this);
	}


	@FindBy(xpath="//button[@class='_2AkmmA _29YdH8']") WebElement elePopUpCloseButton;


	@And("Close the Login Popup")
	public HomePage closeLoginPopup()
	{
		click(elePopUpCloseButton);
		return new HomePage();
	}
}
