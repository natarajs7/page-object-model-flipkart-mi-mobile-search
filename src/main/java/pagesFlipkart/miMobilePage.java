package pagesFlipkart;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class miMobilePage extends ProjectMethods 
{
	public miMobilePage()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(xpath="(//div[@class='_1xHtJz'])[3]") WebElement eleNewestFirst;
	@FindBy(xpath="//div[@class='_3wU53n']") List<WebElement> eleProductNameList; 
	@FindBy(xpath="//div[@class='_1vC4OE _2rQ-NK']") List<WebElement> eleProductPrice;
	@FindBy(xpath="(//div[@class='_3wU53n'])[1]") WebElement eleFirstProductName;

	
	@And("Click the Newest First Link")
	public miMobilePage clickNewestFirstlink()
	{
		click(eleNewestFirst);
		return this;
	}

	@And("Print All the Product Name")
	public miMobilePage printAllProductName() throws InterruptedException
	{
		System.out.println("The Mobile Phones are: ");
		for(WebElement pname: eleProductNameList)
		{	
			System.out.println(pname.getText());
		}
		return this;

	}
	
	@And("Print All the Prize")
	public miMobilePage printAllProductPrize() throws InterruptedException
	{
		System.out.println("The Mobile Phone Prize are: ");
		for(WebElement price: eleProductPrice)
		{	
			System.out.println(price.getText());
		}
		return this;

	}
	
	@And("Click the First Product and Navigate to First Mobile Page")
	public firstMiphonePage clickFirstProduct() throws InterruptedException
	{
		click(eleFirstProductName);
		switchToWindow(1);
		Thread.sleep(2000);
		return new firstMiphonePage();
	}
}

