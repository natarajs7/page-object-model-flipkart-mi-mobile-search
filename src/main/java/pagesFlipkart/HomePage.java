package pagesFlipkart;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import wdMethods.ProjectMethods;

public class HomePage  extends ProjectMethods
{

	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.XPATH, using ="(//span[@class='_1QZ6fC _3Lgyp8'])[1]") WebElement eleElectronicLink;
	@FindBy(linkText="Mi") WebElement eleMi;
	
	
	
	@And("Mouse Over the Electronics Link")
	public HomePage mouseOverElectronicLink()
	{
		mouseOver(eleElectronicLink);
		return this;
	}
	
	
	
	@And("Click MI Link")
	public miMobilePage clickMi() throws InterruptedException 
	{
		click(eleMi);
		//Thread.sleep(2000);
		return new miMobilePage();
	}
}

