package pagesFlipkart;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import cucumber.api.java.en.And;
import cucumber.api.java.en_scouse.An;
import wdMethods.ProjectMethods;

public class firstMiphonePage extends ProjectMethods 
{
	public firstMiphonePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(xpath="//span[@class='_35KyD6']") WebElement eleMobileTextName;
	@FindBy(xpath="//span[@class='_1VpSqZ']/preceding::span[1]") WebElement eleRatings;
	@FindBy(xpath="//span[@class='_1VpSqZ']/following::span[1]") WebElement eleReviews;
	
	
	@And("Verify the Page Title")
	public firstMiphonePage verifyPageTitle() throws InterruptedException
	{
		//verifyTitle(eleMobileTextName.getText());
		verifyTitlepartially("Redmi");
		return this;
	}
	
	
	@And("Print the Rating")
	public firstMiphonePage printRating()
	{
		System.out.println("The Rating Count is: "+getText(eleRatings));
		return this;
	}
	
	
	@And("Print the Reviews")
	public firstMiphonePage printReviews()
	{
		System.out.println("The Review Count is: "+getText(eleReviews)); 
		return this;
	}
	
	@And("Verify the MI Mobile Search is completed")
	public void verifyMobileSearchCompletion()
	{
		System.out.println("The Mobile Search in Flipkart is Successfully Completed");
	}
	
//	public void closeBrowser()
//	{
//		closeAllBrowsers();
//	}
}
