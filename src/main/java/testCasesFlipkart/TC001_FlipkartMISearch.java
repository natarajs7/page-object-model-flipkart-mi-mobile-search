package testCasesFlipkart;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pagesFlipkart.PopUpPage;
import wdMethods.ProjectMethods;

public class TC001_FlipkartMISearch extends ProjectMethods{
	@BeforeTest
	public void setData() 
	{
		testCaseName = "TC001_FlipkartMISearch";
		testDescription = "Search for a MI Mobile in Flipkart";
		authors = "Indhu";
		category = "smoke";
		testNodes = "Leads";
	}
	
	@Test
	public void FlipkartPurchase() throws InterruptedException 
	{
		new PopUpPage()
		.closeLoginPopup()
		.mouseOverElectronicLink()
		.clickMi()
		.clickNewestFirstlink()
		.printAllProductName()
		.printAllProductPrize()
		.clickFirstProduct()
		.verifyPageTitle()
		.printRating()
		.printReviews()
		.closeBrowser();
	}
	
}
