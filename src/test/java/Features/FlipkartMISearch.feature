Feature: Flipkart MI Mobile Phone

#Background: 
#Given Open the Browser
#And Maximize the Browser
#And Set the Timeout
#And Launch the URL

Scenario: Search a MI Mobile Phone in Flipkart and Get the Details
And Close the Login Popup
And Mouse Over the Electronics Link
And Click MI Link
And Click the Newest First Link
And Print All the Product Name
And Print All the Prize
And Click the First Product and Navigate to First Mobile Page
And Verify the Page Title
And Print the Rating
When Print the Reviews
Then Verify the MI Mobile Search is completed