package Steps;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import wdMethods.SeMethods;

public class Hooks extends SeMethods
{
	@Before
	public void before(Scenario sc)
	{
		startResult();
		startTestModule(sc.getName(), sc.getId());	
		test = startTestCase(sc.getName());
		test.assignCategory("Functional");
		test.assignAuthor("Nataraj.S");
		startApp("chrome", "https://www.flipkart.com");		
	}

	@After
	public void after(Scenario sc)
	{
		closeAllBrowsers();
		endResult();
	}
}
