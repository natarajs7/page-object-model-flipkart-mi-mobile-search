/*package Steps;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class FlipkartMIMobileSearchSteps 
{

	ChromeDriver driver;

	@Given("Open the Browser")
	public void openTheBrowser() 
	{
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		driver = new ChromeDriver();
	}


	@Given("Maximize the Browser")
	public void maximizeTheBrowser() 
	{
		driver.manage().window().maximize();
	}


	@Given("Set the Timeout")
	public void setTheTimeout() 
	{
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}


	@Given("Launch the URL")
	public void launchTheURL() 
	{
		driver.get("https://www.flipkart.com");    
	}



	@Given("Close the Login Popup")
	public void closeTheLoginPopup() 
	{
		driver.findElementByXPath("//button[@class='_2AkmmA _29YdH8']").click();
	}


	@Given("Mouse Over the Electronics Link")
	public void mouseOverTheElectronicsLink() 
	{
		WebElement eleElectronicsLink = driver.findElementByXPath("(//span[@class='_1QZ6fC _3Lgyp8'])[1]");
		Actions gundu = new Actions(driver);
		gundu.moveToElement(eleElectronicsLink).build().perform();
	}


	@Given("Click MI Link")
	public void clickMILink() 
	{
		driver.findElementByLinkText("Mi").click();
	}


	@Given("Click the Newest First Link")
	public void clickTheNewestFirstLink() throws InterruptedException 
	{
		driver.findElementByXPath("(//div[@class='_1xHtJz'])[3]").click();
		Thread.sleep(2000);
	}



	@Given("Print All the Product Name")
	public void printAlltheProductName() 
	{
		List<WebElement> ProductNameList = driver.findElementsByXPath("//div[@class='_3wU53n']");

		for (WebElement product : ProductNameList) 
		{
			System.out.println("Product: "+ product.getText());
		}
		System.out.println();
	}



	@Given("Print All the Prize")
	public void printAllthePrize() 
	{
		List<WebElement> PriceList = driver.findElementsByXPath("//div[@class='_1vC4OE _2rQ-NK']");

		for (WebElement prize : PriceList) 
		{
			System.out.println("Price: "+prize.getText());
		}
		System.out.println();
	}


	@Given("Click the First Product and Navigate to First Mobile Page")
	public void clickTheFirstProduct() throws InterruptedException  
	{
		driver.findElementByXPath("(//div[@class='_3wU53n'])[1]").click();
		Set<String> windowList2 = driver.getWindowHandles();
		List<String> SecondList = new ArrayList<>();
		SecondList.addAll(windowList2);
		driver.switchTo().window(SecondList.get(1));
		Thread.sleep(2000);
	}




	@Given("Verify the Page Title")
	public void verifyThePageTitle() 
	{
		String firstProductPageTitle = driver.getTitle();
		System.out.println("The Actual Title is: "+firstProductPageTitle);
		System.out.println();
	}



	@Given("Print the Rating")
	public void printTheRating()
	{
		WebElement eleRatingCount = driver.findElementByXPath("//span[@class='_1VpSqZ']/preceding::span[1]");
		System.out.println("The rating Count is: "+eleRatingCount.getText());
		System.out.println();
	}



	@When("Print the Reviews")
	public void printTheReviews() 
	{
		WebElement eleReviewsCount = driver.findElementByXPath("//span[@class='_1VpSqZ']/following::span[1]");
		System.out.println("The rating Count is: "+eleReviewsCount.getText());
		System.out.println();
	}



	@Then("Verify the MI Mobile Search is completed")
	public void verifyTheMIMobileSearchIsCompleted() 
	{
		System.out.println("The Mobile Search in Flipkart is Successfully Completed");
	}
}
*/