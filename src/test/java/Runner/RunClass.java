package Runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(features="src\\test\\java\\Features\\FlipkartMISearch.feature", glue = {"Steps","pagesFlipkart"}/*dryRun=true, snippets=SnippetType.CAMELCASE,*/,monochrome = true)
public class RunClass 
{

}


